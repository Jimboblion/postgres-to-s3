﻿FROM postgres:latest

ARG GOCRONVER=v0.0.10 
ARG TARGETOS=linux
ARG TARGETARCH=amd64

RUN set -x \
	&& apt-get update && apt-get install -y --no-install-recommends ca-certificates curl python-pip && rm -rf /var/lib/apt/lists/* \
	&& curl -L https://github.com/prodrigestivill/go-cron/releases/download/$GOCRONVER/go-cron-$TARGETOS-$TARGETARCH.gz | zcat > /usr/local/bin/go-cron \
	&& chmod a+x /usr/local/bin/go-cron

RUN pip install s3cmd

ENV POSTGRES_DB="**None**" \
    POSTGRES_HOST="**None**" \
    POSTGRES_PORT=5432 \
    POSTGRES_USER=postgres \
    POSTGRES_PASSWORD="**None**" \
    POSTGRES_EXTRA_OPTS="-Z6" \
    POSTGRES_CLUSTER="FALSE" \
    SCHEDULE="@daily" \
    BACKUP_DIR="/backups" \
    BACKUP_SUFFIX=".sql.gz" \
    BACKUP_KEEP_DAYS=7 \
    BACKUP_KEEP_WEEKS=4 \
    BACKUP_KEEP_MONTHS=6 \
    S3_HOST="s3.amazonaws.com" \
    S3_ACCESS="**None**" \
    S3_SECRET="**None**" \
    S3_LOCATION="US" \
    S3_BUCKET="%(bucket)" \
    HEALTHCHECK_PORT=8080

COPY backup.sh /backup.sh
RUN chmod +x /backup.sh
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["exec /usr/local/bin/go-cron -s \"$SCHEDULE\" -p \"$HEALTHCHECK_PORT\" -- /backup.sh"]

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f "http://localhost:$HEALTHCHECK_PORT/" || exit 1