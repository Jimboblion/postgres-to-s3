# Usage

- set the docker compose image to be `jimboblion/postgres-to-s3:latest`
- Set the environment Variable
    - POSTGRES_DB: <Database Name>
      POSTGRES_HOST: <Container Name>
      POSTGRES_PASSWORD: ${DBPassword}
      S3: <S3 Host>
      S3_ACCESS: ${S3Access}
      S3_SECRET: ${S3Secret}
      S3_LOCATION:  <S3 Region>
      S3_BUCKET:  <S3 Bucket>
- Set the network for the back up to be in the same network as the db


Credit to https://github.com/prodrigestivill/docker-postgres-backup-local 