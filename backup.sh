#!/bin/bash

set -e

if [ "${POSTGRES_DB}" = "**None**" ]; then
  echo "You need to set the POSTGRES_DB environment variable."
  exit 1
fi

if [ "${POSTGRES_HOST}" = "**None**" ]; then
  echo "You need to set the POSTGRES_HOST environment variable."
  exit 1
fi

if [ "${POSTGRES_USER}" = "**None**" ]; then
  echo "You need to set the POSTGRES_USER environment variable."
  exit 1
fi

if [ "${POSTGRES_PASSWORD}" = "**None**" ]; then
  echo "You need to set the POSTGRES_PASSWORD environment variable"
  exit 1
fi

#Initialise S3
echo "[default]" > /root/.s3cfg
echo "access_key = ${S3_ACCESS}" >> /root/.s3cfg
echo "bucket_location = ${S3_LOCATION}" >> /root/.s3cfg
echo "host_base = ${S3}" >> /root/.s3cfg
echo "host_bucket = %(bucket).${S3}" >> /root/.s3cfg
echo "secret_key = ${S3_SECRET}" >> /root/.s3cfg
echo "use_https = True" >> /root/.s3cfg

#Process vars
POSTGRES_DBS=$(echo "${POSTGRES_DB}" | tr , " ")
export PGUSER="${POSTGRES_USER}"
export PGPASSWORD="${POSTGRES_PASSWORD}"
export PGHOST="${POSTGRES_HOST}"
export PGPORT="${POSTGRES_PORT}"
KEEP_DAYS=${BACKUP_KEEP_DAYS}
KEEP_WEEKS=`expr $(((${BACKUP_KEEP_WEEKS} * 7) + 1))`
KEEP_MONTHS=`expr $(((${BACKUP_KEEP_MONTHS} * 31) + 1))`

#Initialize dirs
mkdir -p "${BACKUP_DIR}/daily/" "${BACKUP_DIR}/weekly/" "${BACKUP_DIR}/monthly/"

#Loop all databases
for DB in ${POSTGRES_DBS}; do
  #Initialize filename vers
  DFILE="${BACKUP_DIR}/daily/daily-${DB}-`date +%Y%m%d-%H%M%S`${BACKUP_SUFFIX}"
  WFILE="${BACKUP_DIR}/weekly/weekly-${DB}-`date +%G%V`${BACKUP_SUFFIX}"
  MFILE="${BACKUP_DIR}/monthly/monthly-${DB}-`date +%Y%m`${BACKUP_SUFFIX}"
  #Create dump as daily file
  if [ "${POSTGRES_CLUSTER}" = "TRUE" ]; then
    echo "Creating cluster dump of ${DB} database from ${POSTGRES_HOST}..."
    pg_dumpall -l "${DB}" ${POSTGRES_EXTRA_OPTS} | gzip > "${DFILE}"
  else
    echo "Creating dump of ${DB} database from ${POSTGRES_HOST}..."
    pg_dump -d "${DB}" -f "${DFILE}" ${POSTGRES_EXTRA_OPTS}
  fi
  echo "SQL backup created successfully"
  echo "Uploading to S3..."

  #If Daily File Exists - Overwrite
  s3cmd put ${DFILE} s3://${S3_BUCKET}${DFILE}
  #If !Weekly File Exists - Write Daily to weekly
  set +e
  s3cmd info s3://${S3_BUCKET}${WFILE}
  if [ $? -ne 0 ] ; then
    s3cmd put ${DFILE} s3://${S3_BUCKET}${WFILE}
  fi
  #If !Monthly File Exists - Write Daily to Monthly
  s3cmd info s3://${S3_BUCKET}${MFILE}
  if [ $? -ne 0 ] ; then
    s3cmd put ${DFILE} s3://${S3_BUCKET}${MFILE}
  fi
  set -e
  echo "Uploaded"
done

echo "Cleaning old files..."

#Clean old files
printf -v TODAY '%(%Y-%m-%d)T\n' -1
expire_days=`date '+%C%y-%m-%d' -d "$TODAY-$KEEP_DAYS days"`
expire_weeks=`date '+%C%y-%m-%d' -d "$TODAY-$KEEP_WEEKS days"`
expire_months=`date '+%C%y-%m-%d' -d "$TODAY-$KEEP_MONTHS days"`

s3cmd ls --recursive s3://${S3_BUCKET} \
| while read -r date time size path ; do
  if [[ "$date" < "$expire_months" ]] ; then
    s3cmd del $path
    echo "Deleted $path"
  elif [[ ("$date" < "$expire_weeks") && ($path == *"weekly"*) ]]; then
    s3cmd del $path
    echo "Deleted $path"
  elif [[ ("$date" < "$expire_days") && ($path == *"daily"*) ]]; then
    s3cmd del $path
    echo "Deleted $path"
  fi
done

echo "Backup completed"